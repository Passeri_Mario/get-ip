let scan = document.getElementById("scan");
let loader = document.getElementById("loader");
scan.addEventListener("click", function(){
    scan.style.display="none";
    loader.style.display="block";
    let xhr = new XMLHttpRequest();
    xhr.open('GET', 'get_ip_addresses.php', true);
    xhr.onload = function() {
      if (xhr.status === 200) {
        let ipAddresses = xhr.responseText.split('\n');
        ipAddresses.forEach(function(ipAddress) {
          let li = document.createElement('p');
          li.textContent = ipAddress;
          document.getElementById('ip-list').appendChild(li);
          loader.style.display="none";
        });
      } else {
        console.log('Erreur de chargement : ' + xhr.status);
      }
    };
    xhr.send();
})
